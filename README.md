![](images/audiodice_1_expode.png)
# What it is
Audiodice is a set of 5 speakers comprised of 12 independent drivers each.

[![click](images/audiodice_vimeo.png)](https://vimeo.com/519938720)

<p><a href="https://vimeo.com/519938720">Audiodice - Multidirectional Speakers for Immersive Spaces</a> from <a href="https://vimeo.com/satmetalab">Metalab | SAT</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

# Hardware setup
- 5 audiodices with 12 speakers each (60 independent speakers total)
- 2 RME M-32 DA Pro using MADI connection (64 audio channels total)
- 4 MiniDSP power amplifier: PWR-16
- Furman power conditionner: **Power on** switch on bottom right powers the whole rack.

![](images/audiodice-rack.jpg)

The MADI cable connects to the top RME soundcard, the second one is daisy chained to the first one.

Each audiodice is numbered 1 to 5, on each audiodice speaker is numbered 1 to 12.

## Audiodice channel ordering

Here is how each driver is numbered on an audiodice:

![](images/audiodice_unfolded_face_number.png)

This represent an unfolded dodecahedron, speaker 1, 2, 3 are aiming at the top, 10, 11, 12 at the bottom, 4, 5, 6, 7, 8 and 9 are horizontal.

| Audiodice # | Speaker # | Sound card audio output #|
|-------------|----------:|-------------------------:|
|1|1|1
| |2|2
| |3|3
| |...|...
|2|1|13
||2|14
||...|...
|5|1|49
||2|50
| |...|...
||12|60

# Flattening Audiodice frequency response

We offer a set of filters designed to flatten the frequency response of the audiodice.

More info [here](src/filters/README.md).

# Using with Linux
## Hardware requirements
Currently only PCIe MADI soundcard are supported by Linux.

## Using Audiodice machine
There is a desktop machine installed with Ubuntu 18.04 available. It has a RME PCIe MADI soundcard and a coax cable to connect to the audiodice rack.

Make sure that the current preset loaded in the top RME M-32DA is the MADI preset: *Preset #1*

Audio routing is done with *Jack*.

# Using with mac OS / Windows
## Hardware requirements
A **MADI to USB** or **AVB to USB** soundcard is required.

Those 2 options are tested and working:
- [RME MADIface USB](https://www.rme-usa.com/madiface-usb.html)
- [RME Digiface AVB](https://www.rme-usa.com/digiface-avb.html)

Those 2 options need different configuration on the top RME M-32DA of the rack. Fortunately the sound card offers a preset management system.

## Top RME M32-DA Pro presets
- *Preset #1*: MADI setup (64 MADI inputs -> 64 hardware outputs)
- *Preset #2*: AVB setup (4 AVB stream of 16 channels -> 64 hardware outputs)
- *Preset #3*: Omnidirectional audiodice in a 32 channel dome with MADI. First 32 channels are reserved for the dome, therefore not routed for the audiodice. In order to have an omnidirectional audiodice, you need to duplicate the signal to send on 4 channels according to the table below:

| Audiodice # | Output to route |
|-------------|-----------------|
|1            | 33, 34, 35, 36  |
|2            | 37, 38, 39, 40  |
|3            | 41, 42, 43, 44  |
|4            | 45, 46, 47, 48  |
|5            | 49, 50, 51, 52  |

The hardware routing for this setup is as follow:

Source computer with RME Madiface USB &rarr; RME M-32 DA (Dome 32ch out) &rarr; RME M-32 DA Pro (audiodice rack)

RME Totalmix snapshots for the Madiface USB and Madiface HDSPe can be found [here](src/RME_snapshots/)

## Software requirements
Whatever software that allows driving 60 outputs channels.

# Spatialisation with SATIE
SATIE is [Metalab](https://sat.qc.ca/fr/recherche/metalab)'s spatial audio toolkit. It can be used to perform spatialisation with Audiodice.
https://gitlab.com/sat-metalab/SATIE

# Blueprint

[audiodice_speaker_SAT_V1.pdf](images/audiodice_speaker_SAT_V1.pdf)

# License
This project is released under [CC-BY-NC](https://creativecommons.org/licenses/by-nc/4.0/legalcode).

This project is made possible thanks to the Society for Arts and Technologies (also known as [SAT](https://sat.qc.ca/)).
