[**WORK IN PROGRESS**]
------------------

# Filters
Currently we only provide filters for audiodice #5.

Experience shows that they can be used on other audiodice. It offers a slightly better result compared to no filter at all.

## Omni filtering
Same filter for 12 speakers.
- FILTER_1db_THREE_MICS_IR_CIRMMT_Aud_5_angle_0-48k.wav
- FILTER_3db_THREE_MICS_IR_CIRMMT_Aud_5_angle_0-48k.wav
- FILTER_3db_ONE_MIC_IR_CIRMMT_Aud_5_angle_0-48k.wav
- FILTER_1db_ONE_MIC_IR_CIRMMT_Aud_5_angle_0-48k.wav
- FILTER_3db_SIX_MICS_IR_CIRMMT_Aud_5_angle_0-48k.wav

## Individual speaker filtering
1 filter per speaker.
- Each_spk_individual_correction
- Each_spk_omni_correctiom

We recommend using `FILTER_3db_SIX_MICS_IR_CIRMMT_Aud_5_angle_0-48k.wav` that currently offers the best sounding correction.

# How to use the filters
Those filters are mono IRs. They can be used with any convolution algorithm.

## Filtering with jconvolver
We provide a configuration file to use with jconvolver (a real time convolver for Jack). This configuration uses `FILTER_3db_SIX_MICS_IR_CIRMMT_Aud_5_angle_0-48k.wav`

To run the convolution:

`$ jconvolver -N audiodice5 6_mic_correction.conf`

![](../../images/jack-jconvolver.png)

This command line will start jconvolver, and provide 12 channels of input called `Audiodice-inXX` and 12 channels of processed output called `Audiodice-convdXX`.

In order to filter more than one audiodice, you can start other instances of jconvolver changing the `-N` option to match the name of the corrected audiodice, this will avoid conflicts in jack.

