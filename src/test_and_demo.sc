(
// Get the default server's options
o = Server.default.options;
// Post the number of output channels
o.numOutputBusChannels.postln;
// Set them to a new number
o.numOutputBusChannels = 60; // The next time it boots, this will take effect
// or, if the server was already running, reboot it
Server.default.reboot;
s.makeWindow;
s.meter;
)




(
// automatic panning of a sin, one driver after each other
{
	PanAz.ar(60, SinOsc.ar(200, mul:0.1), pos: LFSaw.kr(0.01), level: 1.0, width: 0.9, orientation: 0.5)
}.play;
)

(
// play periodic sound on a driver selected with mouseX position
{
	PanAz.ar(60,
		Formlet.ar(Impulse.ar(0.5, 0.5), 200 + LFNoise0.kr(1, mul:50), 0.6, 0.4),
		pos: MouseX.kr(minval:-1,maxval:1),
		level: 1.0,
		width: 0.9,
		orientation: 0.5)
}.play;
)

(
// automatic panning of a pulse, one driver after each other
{
	var freq=0.5;
	PanAz.ar(60, Impulse.ar(freq*60, phase:0.5, mul:0.5), pos: LFSaw.kr(freq), level: 1.0, width: 1, orientation: 0.5)
}.play;
)

(
// play periodic impulse on a driver selected with mouseX position
{
	PanAz.ar(60,
		Impulse.ar(1, phase:0.5, mul:0.5),
		pos: MouseX.kr(minval:-1,maxval:1),
		level: 1.0,
		width: 0.9,
		orientation: 0.5)
}.play;
)

(
// constant rain
{ Dust.ar(Array.fill(60, { arg i; 0.1 })); }.play;
)

(
// variable rain density
{
    var maxdensity = 0.2; // x impulse per second * 60 channels
    Dust.ar(Array.fill(60, { arg i; maxdensity * VarSaw.kr(0.1, mul:0.5, add:0.5) }));
}.play;
)


(
// tuning
{
    var maxdensity = 0.05; // impulse per second * 60 channels
    DWGSoundBoard.ar(Pluck.ar(
        WhiteNoise.ar(0.1), Array.fill(60, { arg i; Dust.kr(maxdensity * VarSaw.kr(0.05, mul:0.5, add:0.5))}),
        // WhiteNoise.ar(0.1), Array.fill(60, { arg i; Dust.kr(maxdensity * SinOsc.kr(0.05, 1.5*pi, mul:0.5, add:0.5))}),
        [1763.reciprocal, 878.reciprocal, 440.reciprocal, 221.reciprocal],
        [1763.reciprocal, 878.reciprocal, 440.reciprocal, 221.reciprocal],
        10,
        coef:VarSaw.kr(1, mul:0.1, add:0.1),
        mul: Array.fill(60, { arg i; ExpRand(0.1 , 1.0)})
    ));
}.play;
)
